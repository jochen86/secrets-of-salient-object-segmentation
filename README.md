# README #

This is our code for the paper "***The Secrets of Salient Object Segmentation***" in CVPR 2014

The full package including all datasets is available at [cbi.gatech.edu/salobj](www.cbi.gatech.edu/salobj)

### How do I get set up? ###

* Our script *setup_env.m* in the *code* folder will setup all the environment
* We only support Linux and OSX
* For the full training and testing pipeline, see our script salobj_train_demo.m in the *code* folder
* For testing on a single image using pre-trained model, see our script salobj_test_demo.m in the *code* folder
* We include a pre-trained model in *code/models*. The model is trained using all data in PASCAL-S dataset

### Contribution guidelines ###

* Please submit bug reports via this repository

### Contact ###

* yli440@gatech.edu